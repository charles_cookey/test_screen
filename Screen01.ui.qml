import QtQuick 2.14
import QtQuick.Timeline 1.0
import QtQuick.Layouts 1.0


Rectangle {
    id: root
    width: 1024
    height: 768

    Image {
        id: lastdark
        x: root.width/1.403
        y: 0
        width: root.width/2.13
        height: root.height
        fillMode: Image.Stretch
        source: "landingImages/lastorange.png"
    }

    Image {
        id: white
        x: root.width/1.44
        y: root.height/1.36
        width: root.width/2.6
        height: root.height/3.72
        fillMode: Image.Stretch
        source: "landingImages/whitep.png"

        Image {
            id: dblue
            x: -(root.width/256)
            y: root.height/4.74
            width: root.width/3
            height: root.height/17.9
            fillMode: Image.Stretch
            source: "landingImages/darkblue.png"
        }

        Text {
            id: datetime
            x: root.width/12
            y: root.height/4.4
            color: "#ffffff"
            text: qsTr("29 March |19:00 | Amsterdam, NL")
            font.pixelSize: 15
        }
    }


    Item {
        id: element1
        x: root.width/1.26
        y: root.height/1.4
        width: root.width/5.12
        height: root.height/3.84

        Image {
            id: holland2
            x: root.width/48.76
            y: root.height/19.2
            width: 51
            height: 66
            fillMode: Image.PreserveAspectFit
            source: "landingImages/holland.png"
        }

        Image {
            id: spain
            x: root.width/9.14
            y: root.height/19.2
            width: 105
            height: 68
            fillMode: Image.PreserveAspectFit
            source: "landingImages/spain.png"
        }

        Text {
            id: cntry1
            x: -(root.width/1024)
            y: root.height/6.86
            color: "#243780"
            text: qsTr("Netherlnads")
            font.pixelSize: 15
        }

        Text {
            id: cntry2
            x: root.width/7.06
            y: root.height/6.86
            color: "#243780"
            text: qsTr("Spain")
            font.pixelSize: 15
        }

        Text {
            id: dash
            x: root.width/10.78
            y: root.height/13
            color: "#243780"
            text: qsTr("-")
            font.bold: true
            font.pixelSize: 25
        }
    }

    Image {
        id: dark2
        x: -455.45
        y: 0
        width: root.width/2.2
        height: root.height
        fillMode: Image.Stretch
        source: "landingImages/dorange.png"
    }


    Image {
        id: depandwijn
        x: root.width/2.2
        y: root.height/1.364
        width: root.width/3.21
        height: root.height/3.76
        fillMode: Image.Stretch
        source: "landingImages/depandwijn.png"

        Image {
            id: lblue
            x: -(root.width/256)
            y: root.height/4.74
            width: root.width/3.67
            height: root.height/17.9
            fillMode: Image.Stretch
            source: "landingImages/lightblue.png"
        }

        Text {
            id: afterm
            x: root.width/32
            y: root.height/4.4
            color: "#ffffff"
            text: qsTr("AfterMovie: Netherlans -Estonia")
            font.pixelSize: 15
        }
    }


    Image {
        id: dark1
        x: -354.17
        y: 0
        width: root.width/2.2
        height: root.height
        fillMode: Image.Stretch
        source: "landingImages/dorange.png"
    }


    Image {
        id: light_orange
        x: -(root.width/1.7)
        y: 0
        width: root.width/1.7
        height: root.height
        fillMode: Image.Stretch
        source: "landingImages/lightorange.png"
    }


    Image {
        id: menu
        x: root.width/1.12
        y: -1
        width: root.width/7.54
        height: root.height/13
        fillMode: Image.Stretch
        source: "landingImages/menuh.png"

        Text {
            id: menutext
            x: root.width/27
            y: root.height/51.2
            color: "#ffffff"
            text: qsTr("MENU")
            font.pixelSize: 18
        }
    }



    Image {
        id: holland1
        x: root.width/15.75
        y: root.height/18.73
        width: root.width/21.8
        height: root.height/12.2
        fillMode: Image.Stretch
        source: "landingImages/holland.png"
    }


    Image {
        id: the
        x: root.width/15/75
        y: root.height/5.2
        width: root.width/4.6
        height: root.height/6.3
        fillMode: Image.Stretch
        source: "landingImages/the.png"
    }

    Image {
        id: newtext
        x: root.width/2.35
        y: root.height/3.25
        width: root.width/3.82
        height: root.height/7.4
        fillMode: Image.Stretch
        source: "landingImages/new.png"
    }


    Image {
        id: vandijk
        x: root.width/51.2
        y: root.height/18.7
        width: root.width/2.02
        height: root.height/1.06
        fillMode: Image.Stretch
        source: "landingImages/vandijk.png"
    }


    Image {
        id: wave
        x: root.width/4
        y: root.height/2.12
        width: root.width/2.4
        height: root.height/5.73
        fillMode: Image.Stretch
        source: "landingImages/wave.png"
    }


    MouseArea {
        id: mouseArea
        x: root.width/1.15
        y: root.height/2.7
        width: root.width/10.2
        height: root.height/8.25

        Image {
            id: traingle
            x: 40
            y: 37
            fillMode: Image.PreserveAspectFit
            source: "landingImages/trianglel.png"
        }
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                pingPong: true
                from: 0
                running: true
                to: 1000
                duration: 2000
                loops: -1
            }
        ]
        endFrame: 1000
        startFrame: 0
        enabled: true

        KeyframeGroup {
            target: light_orange
            property: "x"
            Keyframe {
                frame: 0
                value: -(root.width/1.7)
            }

            Keyframe {
                frame: 140
                value: -(root.width/5.17)
            }
        }

        KeyframeGroup {
            target: dark1
            property: "x"
            Keyframe {
                frame: 400
                value: root.width/5.12
            }

            Keyframe {
                frame: 0
                value: -(root.width/2.2)
            }

            Keyframe {
                value: -(root.width/2.2)
                frame: 50
            }
        }

        KeyframeGroup {
            target: dark2
            property: "x"
            Keyframe {
                frame: 400
                value: root.width/2.2
            }

            Keyframe {
                frame: 0
                value: -(root.width/2.2)
            }

            Keyframe {
                value: -(root.width/2.2)
                frame: 25
            }
        }

        KeyframeGroup {
            target: lastdark
            property: "x"
            Keyframe {
                frame: 400
                value: root.width/1.402
            }

            Keyframe {
                frame: 0
                value: -(root.width/2.13)
            }
        }

        KeyframeGroup {
            target: the
            property: "x"
            Keyframe {
                frame: 401
                value: root.width/15.75
            }

            Keyframe {
                frame: 0
                value: -(root.width/10.2)
            }

            Keyframe {
                frame: 200
                value: -(root.width/10.2)
            }
        }

        KeyframeGroup {
            target: the
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 200
                value: true
            }
        }

        KeyframeGroup {
            target: the
            property: "opacity"
            Keyframe {
                frame: 401
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 200
                value: 0
            }
        }

        KeyframeGroup {
            target: newtext
            property: "x"
            Keyframe {
                frame: 450
                value: root.width/2.35
            }

            Keyframe {
                frame: 0
                value: root.width/10.2
            }

            Keyframe {
                frame: 300
                value: root.width/10.2
            }
        }

        KeyframeGroup {
            target: newtext
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 300
                value: true
            }
        }

        KeyframeGroup {
            target: newtext
            property: "opacity"
            Keyframe {
                frame: 450
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 300
                value: 0
            }
        }

        KeyframeGroup {
            target: vandijk
            property: "x"
            Keyframe {
                frame: 450
                value: root.width/51.2
            }

            Keyframe {
                frame: 0
                value: -(root.width/2.03)
            }

            Keyframe {
                frame: 300
                value: -(root.width/2.03)
            }
        }

        KeyframeGroup {
            target: wave
            property: "x"
            Keyframe {
                frame: 501
                value: root.width/4
            }

            Keyframe {
                frame: 0
                value: root.width/68.3
            }

            Keyframe {
                frame: 400
                value: root.width/68.3
            }
        }

        KeyframeGroup {
            target: wave
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 400
                value: true
            }
        }

        KeyframeGroup {
            target: wave
            property: "opacity"
            Keyframe {
                frame: 501
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 400
                value: 0
            }
        }

        KeyframeGroup {
            target: depandwijn
            property: "x"
            Keyframe {
                frame: 700
                value: root.width/2.2
            }

            Keyframe {
                frame: 0
                value: root.width/5.12
            }

            Keyframe {
                frame: 400
                value: root.width/5.12
            }
        }

        KeyframeGroup {
            target: depandwijn
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 400
                value: true
            }
        }

        KeyframeGroup {
            target: white
            property: "x"
            Keyframe {
                frame: 700
                value: root.width/1.44
            }

            Keyframe {
                frame: 0
                value: root.width/2.56
            }

            Keyframe {
                frame: 425
                value: root.width/2.56
            }
        }

        KeyframeGroup {
            target: white
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 425
                value: true
            }
        }

        KeyframeGroup {
            target: rowLayout
            property: "y"
            Keyframe {
                frame: 798
                value: root.height/55
            }

            Keyframe {
                frame: 0
                value: -(root.height/32)
            }

            Keyframe {
                frame: 500
                value: -(root.height/32)
            }
        }

        KeyframeGroup {
            target: menu
            property: "x"
            Keyframe {
                frame: 798
                value: root.width/1.12
            }

            Keyframe {
                frame: 0
                value: root.width
            }

            Keyframe {
                frame: 500
                value: root.width
            }
        }

        KeyframeGroup {
            target: holland1
            property: "y"
            Keyframe {
                frame: 500
                value: -(root.height/12.2)
            }

            Keyframe {
                frame: 0
                value: -(root.height/12.2)
            }

            Keyframe {
                frame: 899
                value: root.height/18.73
            }
        }

        KeyframeGroup {
            target: element1
            property: "y"
            Keyframe {
                frame: 899
                value: root.height/1.36
            }

            Keyframe {
                frame: 0
                value: root.height/1.28
            }

            Keyframe {
                frame: 649
                value: root.height/1.28
            }
        }

        KeyframeGroup {
            target: element1
            property: "visible"

            Keyframe {
                frame: 0
                value: false
            }

            Keyframe {
                frame: 649
                value: true
            }
        }

        KeyframeGroup {
            target: element1
            property: "opacity"
            Keyframe {
                frame: 899
                value: 1
            }

            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 649
                value: 0
            }
        }

        KeyframeGroup {
            target: bscroll
            property: "x"
            Keyframe {
                frame: 949
                value: -(root.width/93)
            }

            Keyframe {
                frame: 0
                value: -(root.width/10.2)
            }

            Keyframe {
                frame: 600
                value: -(root.width/10.2)
            }
        }
    }



    RowLayout {
        id: rowLayout
        x: root.width/1.8
        y: root.height/54.8
        spacing: root.width/25

        Text {
            id: fixtures
            color: "#ffffff"
            text: qsTr("Fixtures")
            font.pixelSize: 18

            MouseArea {
                id: mouseArea_fixtures
                x: -10
                y: -39
                width: 78
                height: 100
            }
        }

        Text {
            id: team
            color: "#ffffff"
            text: qsTr("Team")
            font.pixelSize: 18

            MouseArea {
                id: mouseArea1
                x: -11
                y: -39
                width: 73
                height: 100
            }
        }

        Text {
            id: news
            color: "#ffffff"
            text: qsTr("News")
            font.pixelSize: 18

            MouseArea {
                id: mouseArea2
                x: -17
                y: -39
                width: 79
                height: 100
            }
        }

        Text {
            id: element
            color: "#ffffff"
            text: qsTr("Results")
            font.pixelSize: 18

            MouseArea {
                id: mouseArea3
                x: -11
                y: -39
                width: 85
                height: 100
            }
        }

    }

    Image {
        id: bscroll
        x: -(root.width/93.1)
        y: root.height/1.07
        fillMode: Image.PreserveAspectFit
        source: "landingImages/bottomscroll.png"

        Text {
            id: scrolll
            x: 48
            y: 17
            color: "#ffffff"
            text: qsTr("Scroll")
            font.pixelSize: 17
        }

        Text {
            id: downarrow
            x: 19
            y: 0
            color: "#ffffff"
            text: qsTr("↓")
            font.bold: true
            font.pixelSize: 33
        }
    }



}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.25}D{i:26}
}
##^##*/
